# Intro
Dit zijn alle onderdelen die ik tijdens mijn minor Applied AI heb behandeld.<br>
Resultaat: 8.5


# Applied AI Minor
| **Probleemanalyse en basiskennis** | **Vereiste soort** | **Soort** | **Link naar bewijs** | **Sprint** |
| --- | --- | --- | --- | --- |
| Matrixvermenigvuldigen | T | T | [wiskunde/Matrixwiskunde.ipynb](wiskunde/Matrixwiskunde.ipynb) | 1 |
| Determinant | T | T | [wiskunde/Determinant.ipynb](wiskunde/Determinant.ipynb) | 1 |
| Partieel differentieren | T | T | [wiskunde/Partieel_differentieren.ipynb](wiskunde/Partieel_differentieren.ipynb) | 1 |
| Activatiefuncties | T | T | [wiskunde/Activatiefuncties.ipynb](wiskunde/Activatiefuncties.ipynb) | 1 |
| Inverse matrix | T | T | [wiskunde/Inverse.ipynb](wiskunde/Inverse.ipynb) | 1 |
| Transpose matrix | T | T | [wiskunde/transpose_matrix.ipynb](wiskunde/transpose_matrix.ipynb) | 1 |
| Inproduct | T | T | [wiskunde/inproduct.ipynb](wiskunde/inproduct.ipynb) | 1 |
| Comprehensions | T | T | [python/Comprehensions.ipynb](python/Comprehensions.ipynb) | 1 |
| Dictionaries python | T | T | [python/dictionarP.ipynb](python/dictionarP.ipynb) | 1 |
| Slicing | T | T | [python/Slicing.ipynb](python/Slicing.ipynb) | 1 |
| Supervised vs unsupervised learning | K | K | [Kennis/sup_vs_unsup.ipynb](Kennis/sup_vs_unsup.ipynb) | 1 |
| Clustering | K | K | [Kennis/clustering.ipynb](Kennis/clustering.ipynb) | 1 |
| Stochastic gradient descent | T | T | [wiskunde/gradient_descent.JPEG](wiskunde/gradient_descent.JPEG) | 2 |
| Backpropagation | T | T | [python/backprop.ipynb](python/backprop.ipynb) | 2 |
| Geschikte performance measures kiezen | A | A | [Kennis/performance.ipynb](Kennis/performance.ipynb)<br>[Datasets/wine.ipynb](Datasets/wine.ipynb) | 1 |
| Relatie wiskunde en AI | A | A | [Kennis/wiskunde_ai.ipynb](Kennis/wiskunde_ai.ipynb) | 3 |
| | | | | |
| **Inzicht verkrijgen in data** | **Vereiste soort** | **Soort** | **Link naar bewijs** | **Sprint** |
| Scatter matrix | A | A | [Datasets/wine.ipynb](Datasets/wine.ipynb) | 1 |
| Data visualiseren | A | A | [Datasets/wine.ipynb](Datasets/wine.ipynb) | 1 |
| Normaalverdeling | T | T | [Datasets/avo.ipynb](Datasets/avo.ipynb) | 1 |
| Regressie-analyse | T | T | [Datasets/avo.ipynb](Datasets/avo.ipynb) | 1 |
| | | | | |
| **Data voorbereiden en feature selection** | **Vereiste soort** | **Soort** | **Link naar bewijs** | **Sprint** |
| Z-scores | T | T | [Datasets/deathcause.ipynb](Datasets/deathcause.ipynb) | 1,2 |
| Standaardafwijking | T | T | [Datasets/deathcause.ipynb](Datasets/deathcause.ipynb) | 1 |
| Normaliseren | T | T | [Datasets/deathcause.ipynb](Datasets/deathcause.ipynb) | 1,2 |
| T-toets | A | A | [Datasets/heart.ipynb](Datasets/heart.ipynb) | 1 |
| ANOVA | T | T | [Datasets/heart.ipynb](Datasets/heart.ipynb) | 1,2 |
| Correlatiematrix | A | A | [ML/correlatie matrix.ipynb](ML/correlatie matrix.ipynb) | 1 |
| PCA | T | T | [Datasets/PCA.ipynb](Datasets/PCA.ipynb) | 2 | 
 | | | | |
| **Model selecteren, instellen, trainen en testen/valideren** | **Vereiste soort** | **Soort** | **Link naar bewijs** | **Sprint** |
| Geschikt algoritme kiezen | A | A | [Datasets/wine.ipynb](Datasets/wine.ipynb)| 3 |
| KNN | A | A | [Datasets/wine.ipynb](Datasets/wine.ipynb) | 1 |
| SVM | A | A | [Datasets/CC.ipynb](Datasets/CC.ipynb) | 1 |
| Naive Bayes | A | A |[Datasets/CC.ipynb](Datasets/CC.ipynb) | 1 |
| Linear regression | A | A | [Datasets/wine.ipynb](Datasets/wine.ipynb)| 1 |
| Logistic regression | A | A | [Datasets/CC.ipynb](Datasets/CC.ipynb) | 1 |
| Decision tree | A | A | [Datasets/wine.ipynb](Datasets/wine.ipynb) | 1 |
| Random forest | A | A | [Datasets/wine.ipynb](Datasets/wine.ipynb) | 1 |
| Dense neural network | A | A | [Datasets/CC.ipynb](Datasets/CC.ipynb) | 2 |
| Convolutional neural network | A | A |[Datasets/Natural_Scenes.ipynb](Datasets/Natural_Scenes.ipynb) | 2 |
| Recurrent neural network | T | T | [ML/imdb.ipynb](ML/imdb.ipynb) | 2 |
| Residual neural network | K | K |[Kennis/resnet.ipynb](Kennis/resnet.ipynb) | 2 |
| Autoencoders | T | T | [ML/autoencoder.ipynb](ML/autoencoder.ipynb) | 2 |
| Adversarial learning | T | T | [Datasets/Natural_Scenes.ipynb](Datasets/Natural_Scenes.ipynb) | 3 |
| Reinforcement learning | T | T | [ML/RL.ipynb](ML/RL.ipynb) | 2 |
| | | | | |
| **Model verbeteren** | **Vereiste soort** | **Soort** | **Link naar bewijs** | **Sprint** |
| Cross validation | T | T |[Datasets/wine.ipynb](Datasets/wine.ipynb) | 1 |
| Ensemble methods | T | T |[Datasets/wine.ipynb](Datasets/wine.ipynb) | 2 |
| Transfer learning | T | T | [ML/Transfer_Learning.ipynb](ML/Transfer_Learning.ipynb) | 2 |
| XAI extensies | A | A | [Datasets/CC.ipynb](Datasets/CC.ipynb) | 3 |
| Taalmodellen verfijnen en integreren | T | T | [ML/tweet.ipynb](ML/tweet.ipynb) | 3 | |
 | | | | |
| **Evalueren** | **Vereiste soort** | **Soort** | **Link naar bewijs** | **Sprint** |
| Overfitting en underfitting | A | A |[Datasets/Natural_Scenes.ipynb](Datasets/Natural_Scenes.ipynb) | 1 |
| Learning curve | A | A | [Datasets/Natural_Scenes.ipynb](Datasets/Natural_Scenes.ipynb)| 1 |
| Confusion matrix | A | A | [Datasets/CC.ipynb](Datasets/CC.ipynb)  | 1 |
| ROC curve | A | A | [Datasets/CC.ipynb](Datasets/CC.ipynb) | 1 |
| Testen op bias en betrouwbaarheid | T | T | [Datasets/CC.ipynb](Datasets/CC.ipynb) | 3 |
| | | | | |
| **Toepassingen** | **Vereiste soort** | **Soort** | **Link naar bewijs** | **Sprint** |
| Neural network from scratch | | | [python/nn.ipynb](python/nn.ipynb) | 1 |
| Optimaliseren (gebruik GPU, Cloud, real-time) | T | T | [ML/autoencoder_cloudgpu.ipynb](ML/autoencoder_cloudgpu.ipynb) | 3 |
